﻿<%@ Page Title="Главная adal.kz" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdalService._Default" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>

        .menu-item {
            width: 50%;
            float: left;
        }

        .menu-item > div {
            float: left;
        }

        .menu-item > div:last-child {
            padding: 20px 0 0 5px;
            text-wrap:normal;
            max-width: 215px;
        }

        .menu-item {
            padding: 5px;
        }

        .menu-item::after, .menu-item::before {
            content: "";
            display: table;
        }

        .menu-item::after {
            clear: both;
        }
    </style>

    <div style="margin-top: 60px; max-width: 700px; margin-left: auto; margin-right: auto">
        <div class="menu-item">
            <div><img src="Img/home-icon.png" /></div>
            <div><a runat="server" href="~/Adal/Complex.aspx">Информация по жк/комплексам</a></div>
        </div>
        <div class="menu-item">
            <div><img src="Img/block-icon.png" /></div>
            <div><a runat="server" href="~/Adal/Block.aspx">Информация по блокам и контрольным точкам</a></div>
        </div>
        <div class="menu-item">
            <div><img src="Img/qr-icon.png" /></div>
            <div><a runat="server" href="~/Adal/QRCode.aspx">Печать QR-кодов</a></div>
        </div>
        <div class="menu-item">
            <div><img src="Img/chart-icon.png" /></div>
            <div><a runat="server" href="~/Report/ByDay.aspx">Исполнение графика</a></div>
        </div>
        <div class="menu-item">
            <div><img src="Img/phonebook-icon.png" /></div>
            <div><a runat="server" href="~/Adal/Schedule.aspx">Составление графика</a></div>
        </div>
        <div class="menu-item">
            <div><img src="Img/rain-icon.png" /></div>
            <div><a runat="server" href="~/Adal/JobType.aspx">Типы работ</a></div>
        </div>
    </div>
    
</asp:Content>
