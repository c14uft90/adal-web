﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdalService.Api
{
    public partial class CheckDraft : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            long t = long.Parse(Request.Params["time"]);
            
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(t).ToLocalTime();

            String json = "{\"success\":true}";
            try
            {
                dsCheckAction.SelectParameters["date"].DefaultValue = dtDateTime.ToString();
                dsCheckAction.Select(DataSourceSelectArguments.Empty);

                /*var dataview = (DataView)dsCheckAction.Select(DataSourceSelectArguments.Empty);
                Dictionary<String, object> d = new Dictionary<string, object>();
                foreach (DataRowView row in dataview)
                {
                    d.Add("success", true);
                    d.Add("complex", row["COMPLEX"].ToString());
                    d.Add("block", row["BLOCK"].ToString());
                    d.Add("floor", row["FLOOR"].ToString());
                }

                json = JsonConvert.SerializeObject(d, Formatting.Indented);*/

            }
            catch (Exception ex)
            {
                json = "{\"success\":false, \"message\":\"" + ex.Message + "\"}";
            }

            //var complexId = Request.QueryString["complex_id"].ToString();
            //string json = "{\"name\":\"Joe\"" + complexId + "}";
            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(json);
            Response.End();
        }
    }
}