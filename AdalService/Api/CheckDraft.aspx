﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckDraft.aspx.cs" Inherits="AdalService.Api.CheckDraft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="dsCheckAction" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="[dbo].[CHECKPOINT_DO]" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="check_id" Name="check_id" />
            <asp:QueryStringParameter QueryStringField="qr_id" Name="qr_id" />
            <asp:Parameter Name="date" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
