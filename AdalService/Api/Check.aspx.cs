﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdalService.Api
{
    public partial class Check : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String json = "{\"success\":true}";
            try {
                var dataview = (DataView)dsCheckAction.Select(DataSourceSelectArguments.Empty);
                Dictionary<String, object> d = new Dictionary<string, object>();
                foreach (DataRowView row in dataview)
                {
                    d.Add("success", true);
                    d.Add("complex", row["COMPLEX"].ToString());
                    d.Add("block", row["BLOCK"].ToString());
                    d.Add("floor", row["FLOOR"].ToString());
                }

                json = JsonConvert.SerializeObject(d, Formatting.Indented);

            } catch (Exception ex) {
                json = "{\"success\":false, \"message\":\"" + ex.Message + "\"}";
            }

            //var complexId = Request.QueryString["complex_id"].ToString();
            //string json = "{\"name\":\"Joe\"" + complexId + "}";
            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(json);
            Response.End();
        }
    }
}