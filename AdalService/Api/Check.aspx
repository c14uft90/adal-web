﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Check.aspx.cs" Inherits="AdalService.Api.Check" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="dsCheckAction" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="[dbo].[CHECKPOINT_DO]" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="check_id" Name="check_id" />
            <asp:QueryStringParameter QueryStringField="qr_id" Name="qr_id" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
