﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Schedule.aspx.cs" Inherits="AdalService.Adal.Schedule" %>
<%@ Register assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .selected {
            border: 2px solid black !important;
        }

        .container {
            width: 100% !important;
        }

    </style>
    <script>

        var jobs = [];

        var CellClick = function (s, e) {
            var event = e.htmlEvent;
            var target = event.target;
            var jobTypeId;

            var arr = $(target).parent().attr('class').split(' ');
            for (var i = 0; i < arr.length; i++) {
                var matches = /^adjob_(.*)/.exec(arr[i]);
                if (matches != null) {
                    //console.log(matches[1]);
                    jobTypeId = matches[1];
                }
            }

            if (!event.ctrlKey) {
                $.each(jobs, function (index, el) {
                    $(el.target).removeClass('selected');
                });
                jobs = [];
            }
            
            $(target).addClass('selected');
            jobs.push({ jobTypeId: jobTypeId, target: target, day: $(target).index() });
        }

        var BtnToggleClick = function () {
            var params = jobs.map(function (j) { return j.jobTypeId + '_' + j.day });
            gridMain.PerformCallback(params, function () { jobs = []; gridMain.Refresh(); });
        }

        var ShowJobTypeModal = function () {
            popJobType.Show();
        }

        var BtnJobTypeApplyClick = function () {
            callJobType.PerformCallback('bkmn', function () {
                popJobType.Hide();
                gridMain.Refresh();
            });
        }
    </script>

    <asp:SqlDataSource ID="dsComplex" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="COMPLEX_LIST" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsComplexJob" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="COMPLEX_JOB" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="dtDate" PropertyName="Value" Name="date" />
            <asp:ControlParameter ControlID="ddlComplex" PropertyName="Value" Name="complex_id" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsUpdateJob" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" UpdateCommand="dbo.UPDATE_JOB" UpdateCommandType="StoredProcedure">
        <UpdateParameters>
            <asp:ControlParameter ControlID="dtDate" PropertyName="Value" Name="date" />
            <asp:ControlParameter ControlID="ddlComplex" PropertyName="Value" Name="complex_id" />
            <asp:Parameter Name="job_day_list" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsJobType" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="select * from dbo.s_job_type" SelectCommandType="Text"
        InsertCommand="insert into dbo.T_JOB_TYPE (COMPLEX_ID, JOB_TYPE_ID, REPORT_DATE) values (@complex_id, @job_type_id, @date)" InsertCommandType="Text">
        <InsertParameters>
            <asp:ControlParameter ControlID="ddlComplex" PropertyName="Value" Name="complex_id" />
            <asp:ControlParameter ControlID="dtDate" PropertyName="Value" Name="date" />
            <asp:ControlParameter ControlID="ctl00$MainContent$popJobType$ddlJobType" PropertyName="Value" Name="job_type_id" />
        </InsertParameters>
    </asp:SqlDataSource>


    <table>
        <tr>
            <td>
                <dx:ASPxDateEdit ID="dtDate" runat="server" PickerType="Months" Theme="Metropolis" ClientSideEvents-DateChanged="function(s,e) {
                        gridMain.Refresh();
                        }" Caption="Дата">
                </dx:ASPxDateEdit>
                <dx:ASPxComboBox ID="ddlComplex" runat="server" ValueType="System.String" TextField="NAME" ValueField="ID" DataSourceID="dsComplex" Theme="Metropolis" Caption="Комплекс"></dx:ASPxComboBox>
            </td>
            <td>
                <dx:ASPxButton ID="btnToggle" runat="server" Text="Назначить/Снять" Height="45px" CssClass="ml-2" Theme="Metropolis" AutoPostBack="false" ClientSideEvents-Click="BtnToggleClick" />
            </td>
            <td>
                <dx:ASPxButton ID="btnJobType" runat="server" Text="Тип работ" Height="45px" CssClass="ml-2" Theme="Metropolis" AutoPostBack="false" ClientSideEvents-Click="ShowJobTypeModal" />
            </td>
        </tr>
    </table>
    
    <dx:ASPxGridView ID="gridMain" ClientInstanceName="gridMain" runat="server" Width="100%" OnHtmlRowPrepared="gridMain_HtmlRowPrepared" CssClass="mt-2" OnCustomCallback="gridMain_CustomCallback">
        <ClientSideEvents RowClick="CellClick">
            
        </ClientSideEvents>
        <Settings HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" VerticalScrollableHeight="300" UseFixedTableLayout="True"></Settings>

    </dx:ASPxGridView>

    <dx:ASPxPopupControl ID="popJobType" ClientInstanceName="popJobType" runat="server" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="Тип работ">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <dx:ASPxCallback ID="callJobType" ClientInstanceName="callJobType" runat="server" OnCallback="callJobType_Callback"></dx:ASPxCallback>
                <dx:ASPxComboBox ID="ddlJobType" runat="server" ValueType="System.String" TextField="NAME" ValueField="ID" DataSourceID="dsJobType" Width="500px" Theme="Metropolis"></dx:ASPxComboBox>
                <dx:ASPxButton ID="btnJobTypeApply" runat="server" Text="Добавить" AutoPostBack="false" CssClass="mt-2" Theme="Metropolis" ClientSideEvents-Click="BtnJobTypeApplyClick"></dx:ASPxButton>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
