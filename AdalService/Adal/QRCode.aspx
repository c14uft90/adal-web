﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QRCode.aspx.cs" Inherits="AdalService.Adal.QRCodePage" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        @media print {
            h2 {page-break-before:always}
            body > div {display: none;}
        }

        @page {
            size: auto;
            margin: 0;
        }
    </style>
    <asp:SqlDataSource ID="dsComplex" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="COMPLEX_LIST" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsCheckpointList" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="dbo.CHECKPOINT_LIST" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlComplex" Name="complex_id" PropertyName="Value" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dx:ASPxComboBox ID="ddlComplex" Theme="Metropolis" DataSourceID="dsComplex" runat="server" TextField="NAME" ValueField="ID" ValueType="System.String" ClientSideEvents-SelectedIndexChanged="function(s,e) {
            callQR.PerformCallback();
        }"></dx:ASPxComboBox>

    <dx:ASPxCallbackPanel ID="callQR" ClientInstanceName="callQR" runat="server" Width="700px" OnLoad="callQR_Load" ClientSideEvents-Init="function(){
            callQR.PerformCallback();
        }">
        <PanelCollection>
            <dx:PanelContent>
                <asp:PlaceHolder ID="placeQR" runat="server">

                </asp:PlaceHolder>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

</asp:Content>
