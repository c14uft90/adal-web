﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Complex.aspx.cs" Inherits="AdalService.Adal.Complex" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:SqlDataSource ID="dsComplex" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="COMPLEX_LIST" SelectCommandType="StoredProcedure"
        InsertCommand="dbo.COMPLEX_INSERT" InsertCommandType="StoredProcedure" UpdateCommand="dbo.COMPLEX_UPDATE" UpdateCommandType="StoredProcedure" 
        DeleteCommand="update dbo.S_COMPLEX set is_deleted = 1 where ID = @ID" DeleteCommandType="Text">
        <SelectParameters>
            <asp:ControlParameter ControlID="chkDeleted" PropertyName="value" Name="show_deleted" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="NAME" />
            <asp:Parameter Name="ADDRESS" />
        </InsertParameters>
    </asp:SqlDataSource>

    <h4>Список комплексов</h4>
    <dx:ASPxButton ID="btnAdd" runat="server" Text="Добавить" Theme="Metropolis" AutoPostBack="false" ClientSideEvents-Click="function(s,e) {
            gridMain.AddNewRow();
        }"></dx:ASPxButton>  <dx:ASPxCheckBox ID="chkDeleted" runat="server" Text="Показать удаленные" Layout="UnorderedList" TextAlign="Left" ClientSideEvents-CheckedChanged="function(s,e) {
            gridMain.Refresh();
            }" />
    <dx:ASPxGridView ID="gridMain" ClientInstanceName="gridMain" runat="server" AutoGenerateColumns="False" DataSourceID="dsComplex" KeyFieldName="ID" Width="100%" CssClass="mt-2">
        <SettingsPager Visible="False">
        </SettingsPager>
        <SettingsPopup>
            <HeaderFilter MinHeight="140px"></HeaderFilter>
        </SettingsPopup>
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0" ShowDeleteButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="NAME" VisibleIndex="1" Caption="Наименование">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ADDRESS" VisibleIndex="2" Caption="Адрес">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BLOCK_CNT" VisibleIndex="3" Caption="Кол-во блоков">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="IS_DELETED" VisibleIndex="4" Caption="Удален">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataCheckColumn>
        </Columns>
    </dx:ASPxGridView>
    
</asp:Content>
