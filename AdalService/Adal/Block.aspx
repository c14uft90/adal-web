﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Block.aspx.cs" Inherits="AdalService.Adal.Block" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource ID="dsComplex" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="COMPLEX_LIST" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsBlock" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="dbo.BLOCK_LIST" SelectCommandType="StoredProcedure"
        InsertCommand="dbo.BLOCK_INSERT" InsertCommandType="StoredProcedure" UpdateCommand="dbo.BLOCK_UPDATE" UpdateCommandType="StoredProcedure"
        DeleteCommand="dbo.BLOCK_DELETE" DeleteCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlComplex" Name="complex_id" PropertyName="Value" />
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="ddlComplex" Name="complex_id" PropertyName="Value" />
            <asp:Parameter Name="NAME" />
            <asp:Parameter Name="FLOOR_CNT" />
            <asp:Parameter Name="CHECKPOINTS" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter Name="ID" />
        </DeleteParameters>
    </asp:SqlDataSource>

    <dx:ASPxComboBox ID="ddlComplex" runat="server" DataSourceID="dsComplex" Theme="Metropolis" TextField="NAME" ValueField="ID">
        <ClientSideEvents SelectedIndexChanged="function() {
            gridMain.Refresh();
            }" />
    </dx:ASPxComboBox>
    <dx:ASPxButton ID="btnAdd" runat="server" Text="Добавить" CssClass="mt-2" AutoPostBack="false" Theme="Metropolis" ClientSideEvents-Click="function(s,e) {
            gridMain.AddNewRow();
        }">
    </dx:ASPxButton>
    <dx:ASPxGridView ID="gridMain" ClientInstanceName="gridMain" runat="server" DataSourceID="dsBlock" CssClass="mt-2" Width="100%" KeyFieldName="ID">
        <SettingsPopup>
            <HeaderFilter MinHeight="140px"></HeaderFilter>
        </SettingsPopup>
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0" ShowDeleteButton="true">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" Visible="false" />
            <dx:GridViewDataTextColumn FieldName="COMPLEX_ID" Visible="false" />
            <dx:GridViewDataTextColumn FieldName="COMPLEX" Caption="Комплекс" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="NAME" Caption="Наименование" VisibleIndex="2" />
            <dx:GridViewDataSpinEditColumn FieldName="FLOOR_CNT" Caption="Кол-во этажей" VisibleIndex="3">
                <PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataTextColumn FieldName="CHECKPOINTS" Caption="Контр. точки" />
        </Columns>
    </dx:ASPxGridView>

</asp:Content>
