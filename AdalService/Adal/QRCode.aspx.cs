﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using QRCoder;

namespace AdalService.Adal
{
    public partial class QRCodePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback) {
                ddlComplex.SelectedIndex = 0;
            }
        }

        protected void callQR_Load(object sender, EventArgs e)
        {
            System.Collections.IEnumerable rows = dsCheckpointList.Select(DataSourceSelectArguments.Empty);
            QRCodeGenerator qrGenearator = new QRCodeGenerator();

            if (rows != null){
                var iter = rows.GetEnumerator();
                while (iter.MoveNext()) {
                    var current = iter.Current as System.Data.DataRowView;
                    //Label l = new Label();
                    //l.Text = current.Row["BLOCK"].ToString();
                    //placeQR.Controls.Add(l);
                    placeQR.Controls.Add(GetElement("h2", ddlComplex.Text));
                    placeQR.Controls.Add(GetElement("h3", current.Row["BLOCK"].ToString()));
                    placeQR.Controls.Add(GetElement("h4", "этаж: " + current.Row["FLOOR"].ToString()));

                    QRCodeData qrCodeData = qrGenearator.CreateQrCode(current.Row["ID"].ToString() + ",100", QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                    imgBarCode.Height = 500;
                    imgBarCode.Width = 500;

                    using (Bitmap bitMap = new QRCode(qrCodeData).GetGraphic(20))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            byte[] byteImage = ms.ToArray();
                            imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                        }
                    }

                    placeQR.Controls.Add(imgBarCode);
                }
            }

            /*QRCodeGenerator qrGenearator = new QRCodeGenerator();
            var seed = new Random().Next(50);
            QRCodeData qrCodeData = qrGenearator.CreateQrCode("baukaman" + seed, QRCodeGenerator.ECCLevel.Q);

            QRCode qrCode = new QRCode(qrCodeData);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20);

            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            imgBarCode.Height = 150;
            imgBarCode.Width = 150;

            using (Bitmap bitMap = new QRCode(qrCodeData).GetGraphic(20))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                }
            }

            placeQR.Controls.Add(imgBarCode);*/
        }

        protected Control GetElement(string dom, string message) {
            HtmlGenericControl control = new HtmlGenericControl(dom);
            control.InnerHtml = message;
            return control;
        }
    }
}