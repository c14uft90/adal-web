﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdalService.Adal
{
    public partial class Schedule : BasePage
    {
        int holiday = 0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback) {
                dtDate.Value = DateTime.Now;
                ddlComplex.DataBind();
                ddlComplex.SelectedIndex = 0;
            }

            detectHoliday();

            DataTable dt = new DataTable();
            gridMain.Columns.Clear();

            dt.Columns.Add("Тип работы");
            for (int i = 1; i <= 31; i++)
            {
                dt.Columns.Add(i.ToString());
            }
            dt.Columns.Add("JOB_TYPE_ID");

            foreach (DataColumn dataColumn in dt.Columns)
            {
                GridViewDataTextColumn column = new GridViewDataTextColumn();
                column.FieldName = dataColumn.ColumnName;
                column.Caption = dataColumn.ColumnName;

                if (dataColumn.ColumnName.EndsWith("_ID"))
                {
                    column.Visible = false;
                }

                gridMain.Columns.Add(column);
                column.Width = 40;
            }

            gridMain.Columns[0].FixedStyle = GridViewColumnFixedStyle.Left;
            gridMain.Columns[0].CellStyle.Font.Bold = true;
            gridMain.Columns[0].Width = 250;

            var iter = dsComplexJob.Select(DataSourceSelectArguments.Empty).GetEnumerator();

            object[] row = null;
            
            while (iter.MoveNext())
            {
                var rowLoad = iter.Current as System.Data.DataRowView;
                if (rowLoad["DAY"].Equals(-1))
                {
                    if (row != null)
                        dt.Rows.Add(row);
                    row = new object[dt.Columns.Count];
                    row[0] = rowLoad["JOB_TYPE"];
                    row[row.Length - 1] = rowLoad["JOB_TYPE_ID"];
                }
                else {
                    row[(int)rowLoad["DAY"]] = "1";
                }
            }

            if (row != null)
                dt.Rows.Add(row);

            /*for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                object[] row = new object[dt.Columns.Count];
                row[0] = "JobType" + rowIndex;
                for (int i = 1; i <= 31; i++)
                {
                    row[i] = "";
                }

                dt.Rows.Add(row);
            }*/

            gridMain.DataSource = dt;
            gridMain.DataBind();
        }

        protected void gridMain_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            DataRowView row = (DataRowView) gridMain.GetRow(e.VisibleIndex);
            e.Row.CssClass += " adjob_" + gridMain.GetRowValues(e.VisibleIndex, "JOB_TYPE_ID");
            
            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                if (row != null && i < gridMain.VisibleColumns.Count && row.Row[i].ToString().Equals("1"))
                    e.Row.Cells[i].Style.Add("background-color", "black");

                if ((i - holiday + 7) % 7 == 0) {
                    e.Row.Cells[i].Style.Add("background-color", "#e4e4e4");
                }
            }
        }

        protected void detectHoliday() {
            for (int i = 1; i <= 7; i++)
            {
                var selectedDate = (DateTime)dtDate.Value;
                var d = new DateTime(selectedDate.Year, selectedDate.Month, i);
                if (d.DayOfWeek == DayOfWeek.Sunday)
                {
                    holiday = i;
                    break;
                }
            }
        }

        protected void gridMain_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            dsUpdateJob.UpdateParameters["job_day_list"].DefaultValue = e.Parameters;
            dsUpdateJob.Update();
        }

        protected void callJobType_Callback(object source, CallbackEventArgs e)
        {
            dsJobType.Insert();
        }
    }
}