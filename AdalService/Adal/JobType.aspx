﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobType.aspx.cs" Inherits="AdalService.Adal.JobType" %>
<%@ Register assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="dsJobType" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="select * from dbo.s_job_type" SelectCommandType="Text" 
        InsertCommand="insert into dbo.s_job_type (NAME) values (@name)" InsertCommandType="Text"
        UpdateCommand="update dbo.s_job_type set NAME = @name where ID = @id" UpdateCommandType="Text"
        DeleteCommand="delete dbo.s_job_type where ID = @id"></asp:SqlDataSource>

    <h4>Справочник типов работ</h4>

    <dx:ASPxGridView ID="gridMain" runat="server" AutoGenerateColumns="False" DataSourceID="dsJobType" KeyFieldName="ID" Width="100%" Theme="Metropolis">
        <SettingsPopup>
            <HeaderFilter MinHeight="140px"></HeaderFilter>
        </SettingsPopup>
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn ShowNewButtonInHeader="True" ShowEditButton="true" ShowDeleteButton="true" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="NAME" VisibleIndex="2" Caption="Наименование">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
</asp:Content>
