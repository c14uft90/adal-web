﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ByDay.aspx.cs" Inherits="AdalService.Report.ByDay" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="dsByDay" runat="server" ConnectionString="<%$ ConnectionStrings:adalConnectionString %>" SelectCommand="rep.BY_DATE" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="dtDate" Name="p_date" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxDateEdit ID="dtDate" runat="server" Theme="Metropolis" ClientSideEvents-DateChanged="function(s,e) {
            gridMain.Refresh();
        }" Caption="Дата:"></dx:ASPxDateEdit>

    <dx:ASPxGridView ID="gridMain" ClientInstanceName="gridMain" runat="server" DataSourceID="dsByDay" CssClass="mt-2" Width="100%" OnHtmlDataCellPrepared="gridMain_HtmlDataCellPrepared">
        <SettingsPager Mode="ShowAllRecords" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="COMPLEX" Settings-AllowCellMerge="True" Caption="Комплекс"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BLOCK" Caption="Блок" ></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="FLOOR" Caption="Этаж"></dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="COMPLETED" Caption="Статус"></dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="INSERT_DATE" Caption="Дата">
                <PropertiesTextEdit DisplayFormatString="{0:dd.MM.yyyy HH:mm:sss}" />
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

</asp:Content>
