﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdalService.Report
{
    public partial class ByDay : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback) {
                dtDate.Value = DateTime.Now;
            }

        }

        protected void gridMain_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "COMPLETED")
            {
                if(true.Equals(e.CellValue))
                    e.Cell.BackColor = System.Drawing.Color.FromArgb(200, 255, 200);
                else
                    e.Cell.BackColor = System.Drawing.Color.FromArgb(255, 200, 200);
            }
        }
    }
}