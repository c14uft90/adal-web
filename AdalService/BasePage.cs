﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdalService
{
    public class BasePage : System.Web.UI.Page
    {
        protected override void InitializeCulture()
        {

            //for regional server standards 
            Culture = "ru";
            //for DevExpress localizable strings 
            UICulture = "ru";

        }
    }
}